#include <WiFi.h>
#include <Arduino_JSON.h>
#include <HTTPClient.h>

#define BAUD_RATE 115200
#define TIMEOUT_CONNECTION 5000

// Disclaimer: this serial only accept http scheme
// Disclaimer: https is not supported at the moment

bool connected = false;
HTTPClient httpCli;

void setup() {
  Serial.begin(BAUD_RATE);
}

String statusToString(int code) {
  switch (code) {
    case WL_CONNECTED:
      return "CONNECTED";
      break;
    case WL_NO_SHIELD:
      return "NO_SHIELD";
      break;
    case WL_IDLE_STATUS:
      return "IDLE_STATUS";
      break;
    case WL_NO_SSID_AVAIL:
      return "NO_SSID_AVAIL";
      break;
    case WL_SCAN_COMPLETED:
      return "SCAN_COMPLETED";
      break;
    case WL_CONNECT_FAILED:
      return "CONNECT_FAILED";
      break;
    case WL_CONNECTION_LOST:
      return "CONNECTION_LOST";
      break;
    case WL_DISCONNECTED:
      return "DISCONNECTED";
      break;
    default:
      return "UNKNOWN";
      break;
  }
}

void loop() {
  JSONVar retMessage;
  
  String line = Serial.readString();
  if (line == "") { return; }
  JSONVar inputMessage = JSON.parse(line);

  if (JSON.typeof(inputMessage) != "undefined") {
    String action = (const char*)inputMessage["action"];
    if (action == "networkList") {
      retMessage["action"] = "networkList";
      int count = WiFi.scanNetworks();
      JSONVar networkList;
      for (int i = 0; i < count; i++) {
        JSONVar network;

        network["ssid"] = WiFi.SSID(i);
        network["intensity"] = WiFi.RSSI(i) + 100;
        network["isOpen"] = (WiFi.encryptionType(i) == WIFI_AUTH_OPEN);
        
        networkList[i] = network;
      }
      retMessage["value"] = networkList;
    } else if (action == "connect") {
      if (connected) {
        retMessage["action"] = "error";
        retMessage["description"] = "Chip already connected";
      } else {
        const char* ssid = (const char*) inputMessage["ssid"];
        if (JSON.typeof(inputMessage["password"]) == "undefined") {
          WiFi.begin(ssid);
        } else {
          const char* password = (const char*) inputMessage["password"];
          WiFi.begin(ssid, password);
        }

        int code;
        do {
          delay(TIMEOUT_CONNECTION);
          code = WiFi.status();
        } while (code == WL_IDLE_STATUS);

        if (code != WL_CONNECTED) {
          connected = false;
          retMessage["action"] = "error";
          retMessage["description"] = String("Chip can not connect to ") + String(ssid);
          retMessage["errCode"] = code;
          retMessage["errName"] = statusToString(code);
        } else {
          connected = true;
          retMessage["action"] = "connect";
          retMessage["description"] = "Chip connected";
        }
      }
    } else if (action == "getIp") {
      if (!connected) {
        retMessage["action"] = "error";
        retMessage["description"] = "Chip is not connected";
      } else {
        retMessage["action"] = "getIp";
        retMessage["value"] = WiFi.localIP().toString();
      }
    } else if (action == "disconnect") {
      if (!connected) {
        retMessage["action"] = "error";
        retMessage["description"] = "Chip is not connected";
      } else {
        connected = false;
        WiFi.disconnect();

        retMessage["action"] = "disconnect";
        retMessage["description"] = "Chip is disconnected";
      }
    } else if (action == "httpRequest") {
      if (!connected) {
        retMessage["action"] = "error";
        retMessage["description"] = "Chip is not connected";
      } else {
        String url = (const char*) inputMessage["url"];
        const char* method = (const char*) inputMessage["method"];

        httpCli.begin(url);
        int httpCode;
        if (method != "GET" && JSON.typeof(inputMessage["payload"]) != "undefined") {
          String payload = (const char*) inputMessage["payload"];
          httpCode = httpCli.sendRequest(method, payload);
        } else {
          httpCode = httpCli.sendRequest(method);
        }

        if(httpCode > 0) {
          retMessage["action"] = "httpRequest";
          retMessage["status"] = httpCode;
          retMessage["body"] = httpCli.getString();
        } else {
          retMessage["action"] = "error";
          retMessage["description"] = "Cant fetch request";
          retMessage["errName"] = httpCli.errorToString(httpCode).c_str();
          retMessage["errCode"] = httpCode;
        }
        
        httpCli.end();
      }
    } else {
      retMessage["action"] = "error";
      retMessage["description"] = "Command not found";
    }
  } else {
    retMessage["action"] = "error";
    retMessage["description"] = "Can not parse the input";
  }

  Serial.println(JSON.stringify(retMessage));
}
